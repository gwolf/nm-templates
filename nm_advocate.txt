Hello NAME,

You have either advocated the NM application or sponsored packages of
NM_NAME whom I have been appointed for as Application Manager. Due to
your cooperation with him, you probably know much more about him than I
do. Thus, I would like to ask you to give me a short overview about:

        1) How has the cooperation between you and him worked so far?
        2) How does he handle his tasks?
        3) How does he handle problems?
        4) His social skills? (How does he treat users? Other developers?)
        5) Is he reliable or not?

(Your reply will probably get included verbatim in the final report.)

I thank you in advance for your answer.


Hello NAME,

You have either advocated the NM application or sponsored packages of
NM_NAME whom I have been appointed for as Application Manager. Due to
your cooperation with her, you probably know much more about her than I
do. Thus, I would like to ask you to give me a short overview about:

        1) How has the cooperation between you and her worked so far?
        2) How does she handle her tasks?
        3) How does she handle problems?
        4) Her social skills? (How does she treat users? Other developers?)
        5) Is she reliable or not?

(Your reply will probably get included verbatim in the final report.)

I thank you in advance for your answer.
